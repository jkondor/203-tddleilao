package com.leilaoTDD.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTeste {

    @Test
    public void testarAdicionarNovoLance(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        Leilao leilao = new Leilao(lances);
        leilao.adicionarNovoLance(new Lance(usuario,10.00));

        Lance lance = new Lance(usuario,20.00);
        Assertions.assertEquals(leilao.adicionarNovoLance(lance).getValorDoLance(), 20.00);
        Assertions.assertEquals(leilao.adicionarNovoLance(lance),lance);
    }

    @Test
    public void testarAdicionarNovoLanceMenor(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        Leilao leilao = new Leilao(lances);
        leilao.adicionarNovoLance(new Lance(usuario,10.00));

        Lance lance = new Lance(usuario,5.00);
        Assertions.assertThrows(ArithmeticException.class, () -> {leilao.adicionarNovoLance(lance);});
    }



    @Test
    public void testarValidarLanceInvalido(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        lances.add(new Lance(usuario,5.00));
        lances.add(new Lance(usuario,6.00));
        lances.add(new Lance(usuario,10.00));
        Leilao leilao = new Leilao(lances);

        Lance lance = new Lance(usuario,5.00);

        Assertions.assertEquals(leilao.validarLance(lance), false);
    }
    @Test
    public void testarValidarLanceValido(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        lances.add(new Lance(usuario,5.00));
        lances.add(new Lance(usuario,7.00));
        lances.add(new Lance(usuario,15.00));
        Leilao leilao = new Leilao(lances);

        Lance lance = new Lance(usuario,20.00);

        Assertions.assertEquals(leilao.validarLance(lance), true);
    }

    @Test
    public void testarValidarPrimeiroLance(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        Leilao leilao = new Leilao(lances);

        Lance lance = new Lance(usuario,5.00);

        Assertions.assertEquals(leilao.validarLance(lance), true);
    }

    @Test
    public void testarValidarLanceZero(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        Leilao leilao = new Leilao(lances);

        Lance lance = new Lance(usuario,0.00);

        Assertions.assertEquals(leilao.validarLance(lance), false);
    }
}
