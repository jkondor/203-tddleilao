package com.leilaoTDD.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    @Test
    public void testaRetornarMaiorLance(){

        Usuario usuario = new Usuario(1,"Joao");
        List<Lance> lances = new ArrayList<>();
        lances.add(new Lance(usuario,5.00));
        lances.add(new Lance(usuario,10.00));
        lances.add(new Lance(usuario,6.00));
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Jose", leilao);

        Assertions.assertEquals(leiloeiro.retornarMaiorLance().getValorDoLance(), 10.00);
    }
}
