package com.leilaoTDD.leilao;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public  Lance retornarMaiorLance() {
        Double valorMaiorLance = 0.0;
        Lance maiorlance = new Lance();

        for (Lance lance: leilao.getLances()){

            if (valorMaiorLance < lance.getValorDoLance() ){
                valorMaiorLance = lance.getValorDoLance();
                maiorlance = lance;
           }
        }
        return maiorlance;

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }


}
