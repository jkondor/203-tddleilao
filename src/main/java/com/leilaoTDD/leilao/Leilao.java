package com.leilaoTDD.leilao;

import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance) {

        if (validarLance(lance) ==false){
            throw new ArithmeticException();
        }
        lances.add(lance);
        return lances.get(lances.size()-1);
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }


    public boolean validarLance(Lance lance) {

        if (lance.getValorDoLance() == 0){
            return false;
        }
        for (Lance lanceValida: lances){
            if ( lance.getValorDoLance() < lanceValida.getValorDoLance()){
                return false;
            }
        }
        return true;
    }
}
